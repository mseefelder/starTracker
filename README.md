# StarTracker

## Dependencies

It depends on CImg.h. As the code is written it should be in ../CImg/ in relation to the project root.

To change it, edit the `#include` at *main.cpp*.

Also, it depends on [pyramid](https://gitlab.com/mseefelder/pyramid) that is supposed to be at `../pyramid` (relative).
## Compiling:

To compile, run build-all.sh. It will create an executable named *flow*. 

## Running:

Running it without any parameters gives you instructions
