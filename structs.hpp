#ifndef FLOWSIZE
	#define FLOWSIZE 10
#endif

#include <iostream>
#include <cmath>
#include <algorithm>
#include "../pyramid/pyramid.hpp"

//Two-component vector
struct vec2 {
	float x, y;
	vec2(){}
	vec2(float fx, float fy): x(fx), y(fy) {}
	vec2& operator+=(const vec2& rhs){ 
            x += rhs.x;
            y += rhs.y;
            return *this;
    }
    friend vec2 operator+(vec2 lhs, const vec2& rhs){ 
            lhs += rhs;
            return lhs;
    }
    vec2& operator-=(const vec2& rhs){ 
            x -= rhs.x;
            y -= rhs.y;
            return *this;
    }
    friend vec2 operator-(vec2 lhs, const vec2& rhs){ 
            lhs -= rhs;
            return lhs;
    }
    vec2& operator*=(const vec2& rhs){ 
            x *= rhs.x;
            y *= rhs.y;
            return *this;
    }
    friend vec2 operator*(vec2 lhs, const vec2& rhs){ 
            lhs *= rhs;
            return lhs;
    }
    vec2& operator*=(const float& rhs){ 
            x *= rhs;
            y *= rhs;
            return *this;
    }
    friend vec2 operator*(vec2 lhs, const float& rhs){ 
            lhs *= rhs;
            return lhs;
    }
    vec2& operator/=(const float& rhs){ 
            x /= rhs;
            y /= rhs;
            return *this;
    }
    friend vec2 operator/(vec2 lhs, const float& rhs){ 
            lhs /= rhs;
            return lhs;
    }
};

//Three-component vector
struct vec3 {
	float x;
	float y;
	float z;
	vec3(){}
	vec3(float fx, float fy, float fz): x(fx), y(fy), z(fz){}
	vec3& operator+=(const vec3& rhs){ 
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }
    friend vec3 operator+(vec3 lhs, const vec3& rhs){
    	lhs += rhs;
    	return lhs;
    }
    vec3& operator*=(const vec3& rhs){ 
        x *= rhs.x;
        y *= rhs.y;
        z *= rhs.z;
        return *this;
    }
    friend vec3 operator*(vec3 lhs, const vec3& rhs){
    	lhs *= rhs;
    	return lhs;
    }
    vec3& operator*=(const float& rhs){ 
        x *= rhs;
        y *= rhs;
        z *= rhs;
        return *this;
    }
    friend vec3 operator*(vec3 lhs, const float& rhs){
    	lhs *= rhs;
    	return lhs;
    }
    vec3& operator/=(const float& rhs){ 
        x /= rhs;
        y /= rhs;
        z /= rhs;
        return *this;
    }
    friend vec3 operator/(vec3 lhs, const float& rhs){
    	lhs /= rhs;
    	return lhs;
    }
};

//Stores a tracking point, 
//  its state and its flow history
class point {
private:
	/*
	The flow history is stored in the f array,
	  it works as a circular list
	fc counts how many elements are present in the
	  list;
	fp points to the las position written;
	fc and fp can be only as big as FLOWSIZE and
	  FLOWSIZE-1 respectively;
	*/
	vec2 f[FLOWSIZE]; //flow history
	int fp;//flow history pointer
	int fc;//flow history counter
public:
	vec2 p;
	bool valid;//tracking point state

	//default constructor
	point() {
		fp = -1;
		fc = 0;
		valid = true;
		for (int i = 0; i < FLOWSIZE; ++i)
		{
			f[i].x = 0.0;
			f[i].y = 0.0;
		}
	}

	//Add new flow vector to history, update fc and fp
	void addFlow (vec2 flow) {
		fp = (fp == FLOWSIZE-1) ? 0 : fp+1;
		f[fp].x = flow.x;
		f[fp].y = flow.y;
		if (fc < FLOWSIZE) {
			fc++;
		}
	}

	//get last flow added to history
	inline void getLast (vec2& pFlow) {
		pFlow.x = f[fp].x;
		pFlow.y = f[fp].y;
	}

	//get flow added in the past ('back' elements before)
	//  from history;
	//if back == 0, gets last flow
	void pastFlow (int back, vec2& pFlow) {
		if (fc<=0)
		{
			pFlow.x = 0;
			pFlow.y = 0;
		}
		if (back <= fp) {
			pFlow.x = f[fp-back].x;
			pFlow.y = f[fp-back].y;
		} else {
			pFlow.x = f[FLOWSIZE+fp-back].x;
			pFlow.y = f[FLOWSIZE+fp-back].y;
		}
	}

};

//Stores a frame, with the pyramid
class frame {
private:
	bool hasData, hasPyramid;
	int width, height;
public:
	float* data;
	ImagePyramid<float> pyramid;

	//constructor
	frame (int w, int h) : width(w), height(h) {
		hasData = false;
		hasPyramid = false;
	}

	//set data pointer to data array
	bool setData (float* d) {
		if (hasData) {
			delete [] data;
		}
		data = d;
		hasData = true;
		return true;
	}

	//Check if point p in radius r is accessible in pyramid level plevel
	bool isPointOk (vec2 p, float r, int plevel) {
		float levelWidth = (float) pyramid.getLevelWidth(plevel);
		float levelHeight = (float) pyramid.getLevelHeight(plevel);
		bool valid = true;
		valid = valid && ((p.x+r) < levelWidth);
		valid = valid && ((p.y+r) < levelHeight);
		valid = valid && ((p.x-r) > 0.0);
		valid = valid && ((p.y-r) > 0.0);
		return valid;
	}

	//Get width at pyramid level l
	int getLevelWidth (int l) {
		return pyramid.getLevelWidth(l);
	}

	//Get height at pyramid level l
	int getLevelHeight (int l) {
		return pyramid.getLevelHeight(l);
	}

	//Compute the pyramid for data, with 'levels' levels
	bool computePyramid (int levels) {
		if (hasPyramid) {
			pyramid.updateGaussianPyramidNormalized(data);
		}
		else {
			pyramid.computeGaussianPyramidNormalized(data, levels, width, height, 1);
			hasPyramid = true;
		}
		return true;
	}

	//get point x,y at level l
	float getData (int x, int y) {
		return data[x+y*width];
	}

	//get point x,y at level l
	float getNearest (int x, int y, int l) {
		float* levelData = pyramid.getLevelData(l);
		int levelWidth = pyramid.getLevelWidth(l);
		return levelData[x+y*levelWidth];
	}

	//get non-integer point x,y (linearly interpolated) at level l
	float getLinear (float x, float y, int l) {
		float* levelData = pyramid.getLevelData(l);
		int levelWidth = pyramid.getLevelWidth(l);
		float intX, intY;
		const float fX = std::modf(x, &intX);
		const float fY = std::modf(y, &intY);
		return ((1.-fX)*(1.-fY))*levelData[(int)intX+(int)intY*levelWidth]
			  +((fX)   *(1.-fY))*levelData[(int)intX+1+(int)intY*levelWidth]
			  +((1.-fX)*(fY))   *levelData[(int)intX+(int)intY*levelWidth+levelWidth]
			  +((fX)   *(fY))   *levelData[(int)intX+1+(int)intY*levelWidth+levelWidth];
	}
};