#ifndef LINES
  #define LINES 1
#endif

#include <string>
#include <iostream>
#include "flow.hpp"
#include "../CImg/CImg.h"

int main(int argc, char* argv[]) {

  static const unsigned char pointColor[3] = {255, 0, 0};
  static const unsigned char lineColor[3] = {0, 255, 0};
  static const float multiplier = (float) LINES;

  if (argc < 3) 
  { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
    std::cout << "Usage is: \n"<<argv[0]<<
    " <image path prefix> <extension> <range start> <range end> <number of digits> <pyramid levels>\n"<<
    "example: "<<argv[0]<<" ../data/frame .jpg 0 500 4 3\n"<<
    "will read from ../data/frame0000.jpg to ../data/frame0500.jpg\n"<<
      "Press any key to close... \n";
      std::cin.get();
      return 0;
  }

  std::string namePrefix(argv[1]);
  std::string extension(argv[2]);
  int rangeStart = std::stoi(argv[3]);
  int rangeEnd = std::stoi(argv[4]);
  int digitsnum = std::stoi(argv[5]);
  int pyramidLevels = std::stoi(argv[6]);

  std::string numberOfFile = std::to_string(rangeStart);
  for (int l = numberOfFile.size(); l < digitsnum; l++)
  {
    numberOfFile = "0" + numberOfFile;
  }
  std::string fileName = namePrefix + numberOfFile + extension;
  std::string saveName;

  cimg_library::CImg<float> img1, img2, ROI;
  img1.assign(fileName.c_str());
  ROI.assign(img1);

  Flow flow;
  flow.initialize(img1.data(), img1.width(), img1.height(), img1.spectrum(), pyramidLevels);

  //choose Region Of Interest
  bool done = false;
  int firstX, firstY, secondX, secondY;
  while(!done){
    bool first = false;
    bool second = false;
    bool accept = false;

    ROI.assign(img1);
    cimg_library::CImgDisplay img1_disp(ROI, "Region of Interest: Click a point");
    
    while (!img1_disp.is_closed()) { 
      img1_disp.wait();
      if (img1_disp.button()&1) {
        firstX = img1_disp.mouse_x();
        firstY = img1_disp.mouse_y();
        first = true;
        img1_disp.close();
      }
    }
    img1_disp.flush();
    
    if (!first)
    {
      std::cout<<"Window closed"<<std::endl;
      return 0;
    }
    
    img1_disp.set_title("Click another point. X to undo.");
    ROI.draw_circle(firstX, firstY, 1, pointColor).display(img1_disp);
    img1_disp.show();
    
    while (!img1_disp.is_closed()) { 
      img1_disp.wait();
      if (img1_disp.button()&1) {
        secondX = img1_disp.mouse_x();
        secondY = img1_disp.mouse_y();
        second = true;
        img1_disp.close();
      } else if (img1_disp.is_keyX()) {
        second = true;
        first = false;
        img1_disp.close();
      }
    }
    img1_disp.flush();
    
    if (second) {
      if (!first) {
        continue;
      }
    } else {
      std::cout<<"Window closed"<<std::endl;
      return 0;
    }
    
    img1_disp.set_title("Enter to accept. X to undo.");
    ROI.draw_circle(secondX, secondY, 1, pointColor);
    ROI.draw_rectangle(
      std::min(firstX, secondX), 
      std::min(firstY, secondY),
      std::max(firstX, secondX), 
      std::max(firstY, secondY),
      lineColor,
      0.3).display(img1_disp);
    img1_disp.show();
    while (!img1_disp.is_closed()) {
      img1_disp.wait();
      if (img1_disp.is_keyENTER()) {
        accept = true;
        done = true;
        img1_disp.close();
      } else if (img1_disp.is_keyX()) {
        accept = true;
        img1_disp.close();
      }
    }
    img1_disp.flush();

    if (!accept)
    {
      std::cout<<"Window closed"<<std::endl;
      return 0;
    }
  }

  flow.choosePoints(0.1, firstX, firstY, secondX, secondY);

  for (int i = rangeStart+1; i <= rangeEnd; i++)
  {
    numberOfFile = std::to_string(i);
    for (int l = numberOfFile.size(); l < digitsnum; l++)
    {
      numberOfFile = "0" + numberOfFile;
    }
    fileName = namePrefix + numberOfFile + extension;
    saveName = "save" + numberOfFile + extension;

    const char * c = fileName.c_str();
    img2.assign(c);

    flow.trackROI(img2.data());

    //Print debug lines
    vec2 pastFlow(0., 0.);
    vec2 pastOrigin(0,0);
    
    for (int i = 0; i < flow.getPointCount(); ++i)
    {
      if (!flow.isPointValid(i))
      {
        continue;
      }
      //draw start
      flow.getPastFlow(i, 0, pastFlow);
      img2.draw_line(flow.getPointX(i), flow.getPointY(i), 
            flow.getPointX(i)-pastFlow.x*multiplier, 
            flow.getPointY(i)-pastFlow.y*multiplier, 
            lineColor);
      pastOrigin.x = flow.getPointX(i) - pastFlow.x;
      pastOrigin.y = flow.getPointY(i) - pastFlow.y;

      for (int j = 1; j < FLOWSIZE; ++j)
      {
        flow.getPastFlow(i, j, pastFlow);
        img2.draw_line(pastOrigin.x, pastOrigin.y, 
          pastOrigin.x-pastFlow.x*multiplier, 
          pastOrigin.y-pastFlow.y*multiplier, 
          lineColor);
        pastOrigin.x -= pastFlow.x;
        pastOrigin.y -= pastFlow.y;
      }

      img2.draw_circle(flow.getPointX(i), flow.getPointY(i), 1, pointColor);
    }

    flow.getROI(firstX, firstY, secondX, secondY);
    img2.draw_rectangle(
      firstX, 
      firstY,
      secondX, 
      secondY,
      lineColor,
      0.3);

    img2.save_png(saveName.c_str());

  }

  //display final image
  /**
  cimg_library::CImgDisplay img1_disp(img2, "Flow");
  while (!img1_disp.is_closed()) { 
  }
  /**/
  #ifdef DEBUG
    std::cout<<"Closed Window!"<<std::endl;
  #endif

  return 0;
}