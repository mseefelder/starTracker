#include "structs.hpp"

//ROI stands for Region of Interest

class Flow{
public:
	Flow() {}

	~Flow() {

	}

	void initialize(float* firstFrame, int w, int h, int s, int l);

	//Choos good tracking points, without or with ROI, respectively
	void choosePoints(float threshold);
	void choosePoints(float t, int& x1, int& y1, int& x2, int& y2);

	//Compute flow and update ROI position
	void trackROI(float* nextFrame);

	//Get flow 
	void getPastFlow(int i, int back, vec2& pastFlow);

	//Get x coordinate of point i
	float getPointX(int i);

	//Get y coordinate of point i
	float getPointY(int i);

	//Check if point i is valid
	bool isPointValid(int i);

	//Return total # of points
	int getPointCount();

	//return ratio of valid points relative to total points
	//Interval: [0,1]
	float validPointRatio();

	//Set function inputs tu ROI corner coordinates
	void getROI(int& minx, int& miny, int& maxx, int& maxy);

private:
	//General variables, set on initialization
	int width, height, spectrum, pyramidLevels, baseSize;
	
	//Frames
	frame* frameA;
	frame* frameB;

	//Points
	int totalPointCount;
	int validPointCount;
	point* pointList;
	float threshold;

	//Region of Interest
	vec2 ROImin;
	vec2 ROImax;

	//Set next frame, compute flow and swap frames
	void computeFlow(float* nextFrame);

	//compute pyramid flow
	bool pyramidFlow (vec2& flow, bool remOutliers);

	//update region of interest position
	void updateROI ();

};