#include "flow.hpp"

float getMinEigenValue2x2(float& matA, float& matB, float& matC, float& matD);
bool desaturate(float* input, frame* output, int width, int height, int spectrum);

void Flow::initialize(float* firstFrame, int w, int h, int s, int l) {
	width = w; height = h; spectrum = s; pyramidLevels = l;
	baseSize = w*h;

	frameA = new frame(width, height);
	frameB = new frame(width, height);

	desaturate(firstFrame, frameA, width, height, spectrum);
	frameA->computePyramid(pyramidLevels);
}

//Creates pointList, sets total and validPointCount
//Chooses a good set of tracking points for the whole frame
void Flow::choosePoints(float t) {
	//allocate needed arrays
	float* A = new float[baseSize*3];
	float* B = new float[baseSize*3];
	float* eigenValues = new float[baseSize];
	bool* points = new bool[baseSize];
	//zero-out all elements
	for (int i = 0; i < baseSize; ++i) {
		A[i] = 0.0; A[i+baseSize] = 0.0; A[i+baseSize+baseSize] = 0.0;
		B[i] = 0.0; B[i+baseSize] = 0.0; B[i+baseSize+baseSize] = 0.0;
		eigenValues[i] = 0.0;
		points[i] = 0.0;
	}
	//calculate gradient
	int index = 0;
	for (int y = 1; y < height-1; ++y) {
		for (int x = 1; x < width-1; ++x) {
			index = y*width+x;
			A[index] = 
				0.5 * (frameA->getData(x+1, y) - frameA->getData(x-1, y)); //Ix
			A[index+baseSize] = 
				0.5 * (frameA->getData(x, y+1) - frameA->getData(x, y-1)); //Iy
		}
	}
	//get matrix coefficients from gradient
	for (int y = 1; y < height-1; ++y) {
		for (int x = 1; x < width-1; ++x) {
			index = y*width+x;
			B[index] = 
				A[index]*A[index];//IxIx
			B[y*width+x+baseSize] = 
				A[index]*A[index+baseSize];//IxIy
			B[index+baseSize+baseSize] = 
				A[index+baseSize]*A[index+baseSize];//IyIy
		}
	}
	//matrix sum
	for (int y = 2; y < height-2; ++y) {
		for (int x = 2; x < width-2; ++x) {
			for (int layer = 0; layer < 3; ++layer) {
				index = layer*baseSize+y*width+x;
				A[index] =  B[index];//x,y
				A[index] += B[index-1];//x-1,y
				A[index] += B[index+1];//x+1,y
				A[index] += B[index-width];//x,y-1
				A[index] += B[index+width];//x,y+1
				A[index] += B[index-1-width];//x-1, y-1
				A[index] += B[index-1+width];//x-1,y+1
				A[index] += B[index+1-width];//x+1,y-1
				A[index] += B[index+1+width];//x+1,y+1
			}
		}
	}
	//get point list -> pointList

	//Get maximum value among minimum eigenvalues
	float maxEigenValue = -1.0;
	float minEigenValue = 0.0;
	float a = 0.0;
	float bc = 0.0;
	float d = 0.0;
	for (int y = 2; y < height-2; ++y) {
		for (int x = 2; x < width-2; ++x) {
			index = y*width+x;
			a = A[index];
			bc = A[index+baseSize];
			d = A[index+baseSize+baseSize];

			minEigenValue = getMinEigenValue2x2(a, bc, bc, d);
			maxEigenValue = std::max(maxEigenValue, minEigenValue);
			eigenValues[index] = minEigenValue;
		}
	}

	threshold = t;
	maxEigenValue = maxEigenValue*threshold;

	//keep points with minimum eigenvalue > maxEigenValue
	for (int y = 2; y < height-2; ++y) {
		for (int x = 2; x < width-2; ++x) {
			points[y*width+x] = (eigenValues[y*width+x] > maxEigenValue) ? true : false;
		}
	}

	//keep only local maxima (8-connected)
	bool diff = true;
	int pointCount = 0;
	for (int y = 2; y < height-2; ++y) {
		for (int x = 2; x < width-2; ++x) {
			index = y*width+x;
			if (points[index] == true){
				diff = true;
				diff = diff && (eigenValues[index]-eigenValues[index-width-1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-width]   > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-width+1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-1]       > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+1]       > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width-1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width]   > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width+1] > 0.0) ? true : false;
				
				pointCount += (diff == true) ? 1 : 0;
				points[index] = diff;
			}
		}
	}

	//create pointList
	pointList = new point[pointCount];
	totalPointCount = pointCount;
	validPointCount = pointCount;

	//fill pointList
	int pointIndex = 0;
	for (int y = 2; y < height-2; ++y){
		for (int x = 2; x < width-2; ++x){
			if (points[y*width+x] == true){
				pointList[pointIndex].p.x = (float)x;
				pointList[pointIndex].p.y = (float)y;
				pointIndex++;
			}
		}
	}

	//deallocate arrays
	delete [] A;
	delete [] B;
	delete [] eigenValues;
	delete [] points;
}

//Creates pointList, sets total and validPointCount
//Chooses a good set of tracking points for the area delimited by
//  the Region of Interest: rectangle with opposite corners on
//  (x1,y1)  (x2,y2).
void Flow::choosePoints(float t, int& x1, int& y1, int& x2, int& y2) {
	//Set region of interest limited by image
	ROImin.x = (float) std::max(std::min(x1, x2), 1);
	ROImin.y = (float) std::max(std::min(y1, y2), 1);
	ROImax.x = (float) std::min(std::max(x1, x2), width-1);
	ROImax.y = (float) std::min(std::max(y1, y2), width-1);

	//integer counterparts for ROI
	const int rx = (int) ROImin.x;
	const int ry = (int) ROImin.y;
	const int Rx = (int) ROImax.x;
	const int Ry = (int) ROImax.y;

	//allocate needed arrays
	float* A = new float[baseSize*3];
	float* B = new float[baseSize*3];
	float* eigenValues = new float[baseSize];
	bool* points = new bool[baseSize];
	//zero-out all elements
	for (int i = 0; i < baseSize; ++i) {
		A[i] = 0.0; A[i+baseSize] = 0.0; A[i+baseSize+baseSize] = 0.0;
		B[i] = 0.0; B[i+baseSize] = 0.0; B[i+baseSize+baseSize] = 0.0;
		eigenValues[i] = 0.0;
		points[i] = 0.0;
	}
	//calculate gradient
	int index = 0;
	for (int y = ry; y < Ry; ++y) {
		for (int x = rx; x < Rx; ++x) {
			index = y*width+x;
			A[index] = 
				0.5 * (frameA->getData(x+1, y) - frameA->getData(x-1, y)); //Ix
			A[index+baseSize] = 
				0.5 * (frameA->getData(x, y+1) - frameA->getData(x, y-1)); //Iy
		}
	}
	//get matrix coefficients from gradient
	for (int y = ry; y < Ry; ++y) {
		for (int x = rx; x < Rx; ++x) {
			index = y*width+x;
			B[index] = 
				A[index]*A[index];//IxIx
			B[y*width+x+baseSize] = 
				A[index]*A[index+baseSize];//IxIy
			B[index+baseSize+baseSize] = 
				A[index+baseSize]*A[index+baseSize];//IyIy
		}
	}
	//matrix sum
	for (int y = ry+1; y < Ry-1; ++y) {
		for (int x = rx+1; x < Rx-1; ++x) {
			for (int layer = 0; layer < 3; ++layer) {
				index = layer*baseSize+y*width+x;
				A[index] =  B[index];//x,y
				A[index] += B[index-1];//x-1,y
				A[index] += B[index+1];//x+1,y
				A[index] += B[index-width];//x,y-1
				A[index] += B[index+width];//x,y+1
				A[index] += B[index-1-width];//x-1, y-1
				A[index] += B[index-1+width];//x-1,y+1
				A[index] += B[index+1-width];//x+1,y-1
				A[index] += B[index+1+width];//x+1,y+1
			}
		}
	}
	//get point list -> pointList

	//Get maximum value among minimum eigenvalues
	float maxEigenValue = -1.0;
	float minEigenValue = 0.0;
	float a = 0.0;
	float bc = 0.0;
	float d = 0.0;
	for (int y = ry+1; y < Ry-1; ++y) {
		for (int x = rx+1; x < Rx-1; ++x) {
			index = y*width+x;
			a = A[index];
			bc = A[index+baseSize];
			d = A[index+baseSize+baseSize];

			minEigenValue = getMinEigenValue2x2(a, bc, bc, d);
			maxEigenValue = std::max(maxEigenValue, minEigenValue);
			eigenValues[index] = minEigenValue;
		}
	}

	threshold = t;
	maxEigenValue = maxEigenValue*threshold;

	//keep points with minimum eigenvalue > maxEigenValue
	for (int y = ry+1; y < Ry-1; ++y) {
		for (int x = rx+1; x < Rx-1; ++x) {
			points[y*width+x] = (eigenValues[y*width+x] > maxEigenValue) ? true : false;
		}
	}

	//keep only local maxima (8-connected)
	bool diff = true;
	int pointCount = 0;
	for (int y = ry+1; y < Ry-1; ++y) {
		for (int x = rx+1; x < Rx-1; ++x) {
			index = y*width+x;
			if (points[index] == true){
				diff = true;
				diff = diff && (eigenValues[index]-eigenValues[index-width-1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-width]   > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-width+1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index-1]       > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+1]       > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width-1] > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width]   > 0.0) ? true : false;
				diff = diff && (eigenValues[index]-eigenValues[index+width+1] > 0.0) ? true : false;
				
				pointCount += (diff == true) ? 1 : 0;
				points[index] = diff;
			}
		}
	}

	//create pointList
	pointList = new point[pointCount];
	totalPointCount = pointCount;
	validPointCount = pointCount;

	//fill pointList
	int pointIndex = 0;
	for (int y = ry+1; y < Ry-1; ++y) {
		for (int x = rx+1; x < Rx-1; ++x) {
			if (points[y*width+x] == true){
				pointList[pointIndex].p.x = (float)x;
				pointList[pointIndex].p.y = (float)y;
				pointIndex++;
			}
		}
	}

	//deallocate arrays
	delete [] A;
	delete [] B;
	delete [] eigenValues;
	delete [] points;
}

//Make grayscale version of next frame,
//  compute its pyramid and
//  calculate flow from current to next frame
void Flow::computeFlow(float* nextFrame) {
	vec2 flow(0,0);
	bool success;
	success = desaturate(nextFrame, frameB, width, height, spectrum);
	success = success && frameB->computePyramid(pyramidLevels);
	success = success && pyramidFlow(flow, true);
	
	if (success) {
		//swap frames
		frame* temp = frameA;
		frameA = frameB;
		frameB = temp;
	}
}

float Flow::validPointRatio() {
	return ((float) validPointCount)/((float) totalPointCount);
}

void Flow::trackROI(float* nextFrame) {
	computeFlow(nextFrame);
	updateROI();
}

void Flow::updateROI() {
	//calculate mean flow vector
	vec2 meanFlow(0,0);
	vec2 flow(0,0);
	for (int i = 0; i < totalPointCount; ++i)
	{
		if(pointList[i].valid){
			pointList[i].getLast(flow);
			meanFlow.x += flow.x;
			meanFlow.y += flow.y;
		}
	}
	const float multiplier = 1.0/validPointCount;
	meanFlow.x *= multiplier;
	meanFlow.y *= multiplier;

	//move ROI
	ROImax.x += meanFlow.x;
	ROImax.y += meanFlow.y;
	ROImin.x += meanFlow.x;
	ROImin.y += meanFlow.y;
}

void Flow::getROI(int& minx, int& miny, int& maxx, int& maxy) {
	maxx = (int) ROImax.x;
	maxy = (int) ROImax.y;
	minx = (int) ROImin.x;
	miny = (int) ROImin.y;
}

bool Flow::pyramidFlow (vec2& flow, bool remOutliers) {
	vec2* lastFlow = new vec2[totalPointCount];
	vec2 point(0,0);
	int index, levelWidth, levelHeight;
	float Ix, Iy, IxIx, IxIy, IyIy, determinant, It, IxIt, IyIt, tempFlowX, tempFlowY;

	//fill lastFlow with zeroed-out vec2s
	for (int i = 0; i < totalPointCount; ++i)
	{
		lastFlow[i].x = 0.0;
		lastFlow[i].y = 0.0;
	}

	for (int level = pyramidLevels-1; level >= 0; --level)
	{
		index = 0;
		levelWidth = frameA->getLevelWidth(level);
		levelHeight = frameA->getLevelHeight(level);

		for (int i = 0; i < totalPointCount; ++i)
		{
			if (!pointList[i].valid)
			{
				continue;
			}
			//multiply last flow by 2
			lastFlow[i] += lastFlow[i];
			//Find point on this level
			point.x = pointList[i].p.x;
			point.y = pointList[i].p.y;
			for (int d = 0; d < level; ++d)
			{
				point.x *= 0.5;
				point.y *= 0.5;
			}
			//Check if point is valid
			if(!frameA->isPointOk(point, 2, level) || !frameB->isPointOk(point+lastFlow[i], 2, level)){
				pointList[i].valid = false;
				validPointCount--;
				continue;
			}
			//Find AtA
			Ix = 0; Iy = 0; IxIx = 0; IxIy = 0; IyIy = 0;

			for (int y = -1; y <= 1; ++y) {
				for (int x = -1; x <= 1; ++x) {
					Ix = 0.5 * ( 
						frameA->getLinear(point.x+(float)x+1.0, point.y+(float)y, level) 
						-
						frameA->getLinear(point.x+(float)x-1.0, point.y+(float)y, level)
					);
					Iy = 0.5 * ( 
						frameA->getLinear(point.x+(float)x, point.y+(float)y+1.0, level) 
						-
						frameA->getLinear(point.x+(float)x, point.y+(float)y-1.0, level)
					);
					IxIx += Ix*Ix;
					IxIy += Ix*Iy;
					IyIy += Iy*Iy;
				}
			}
			//Invert AtA
			determinant = (IxIx*IyIy - IxIy*IxIy);
			if(determinant == 0.0){
				pointList[i].valid = false;
				validPointCount--;
				continue;
			}
			determinant = 1.0/determinant;
			IxIx *= determinant;
			IxIy *= -determinant;
			IyIy *= determinant;

			//Find Atb
			Ix = 0; Iy = 0; It = 0; IxIt = 0; IyIt = 0;

			for (int y = -1; y <= 1; ++y) {
				for (int x = -1; x <= 1; ++x) {
					It = frameB->getLinear(point.x+(float)x+lastFlow[i].x, point.y+(float)y+lastFlow[i].y, level)
						-
						frameA->getLinear(point.x+(float)x, point.y+(float)y, level);
					Ix = 0.5 * ( 
						frameA->getLinear(point.x+(float)x+1.0, point.y+(float)y, level) 
						-
						frameA->getLinear(point.x+(float)x-1.0, point.y+(float)y, level)
					);
					Iy = 0.5 * ( 
						frameA->getLinear(point.x+(float)x, point.y+(float)y+1.0, level) 
						-
						frameA->getLinear(point.x+(float)x, point.y+(float)y-1.0, level)
					);
					IxIt += Ix*It;
					IyIt += Iy*It;
				}
			}
			IxIt = -IxIt;
			IyIt = -IyIt;

			//Solve system
			tempFlowX = IyIy*IxIt + IxIy*IyIt;
			tempFlowY = IxIy*IxIt + IxIx*IyIt;
			if(std::isnan(tempFlowX) || std::isnan(tempFlowY) || std::isinf(tempFlowX) || std::isinf(tempFlowY)){
				pointList[i].valid = false;
				validPointCount--;
				continue;
			}
			lastFlow[i].x += tempFlowX;
			lastFlow[i].y += tempFlowY;
		}
	}

	//add flows to flow history and update points
	float meanFlowX = 0;
	float meanFlowY = 0;
	for (int i = 0; i < totalPointCount; ++i) {
		if (pointList[i].valid) {
			pointList[i].p.x += lastFlow[i].x;
			pointList[i].p.y += lastFlow[i].y;
			pointList[i].addFlow(lastFlow[i]);

			meanFlowX += lastFlow[i].x;
			meanFlowY += lastFlow[i].y;
		}
	}

	#ifdef DEBUG
		std::cout<<"Mean Flow: "<<meanFlowX/(float)validPointCount<<
			", "<<meanFlowY/(float)validPointCount<<std::endl;
	#endif

	delete [] lastFlow;

	return true;
}

void Flow::getPastFlow(int i, int back, vec2& pastFlow) {
	pointList[i].pastFlow(back, pastFlow);
}

float Flow::getPointX(int i) {
	return pointList[i].p.x;
}

float Flow::getPointY(int i) {
	return pointList[i].p.y;	
}

int Flow::getPointCount() {
	return totalPointCount;
}

bool Flow::isPointValid(int i) {
	return pointList[i].valid;
}

bool desaturate(float* input, frame* output, int width, int height, int spectrum) {	
	const long long size = width*height;
	float pix = 0.;
	float* data = new float[size];

	for (int i = 0; i < size; ++i)
	{
		pix = .0;
		for (int j = 0; j < spectrum; ++j)
		{
	  		pix += input[i+size*j];
		}
		pix /= (float)spectrum;
		pix /= 255.;
		data[i] = pix;
	}
	
	return output->setData(data);
}

//Get minimum eigen value for 2x2 matrix of the form
//  {{matA,matB},{matC,matD}}
float getMinEigenValue2x2(float& matA, float& matB, float& matC, float& matD) {
	float b = matA+matD;
	float c = matA*matD - matB*matC;

	//b^2 - 4ac, where a=1.0
	const float delta = b*b - 4*c;
	if (delta < 0)
		return -1.0;

	float ev1 = 0.0;
	float ev2 = 0.0;
	float sqrtDelta = std::sqrt(delta);
	
	ev1 = b - sqrtDelta;
	ev2 = b + sqrtDelta;

	ev1 *= 0.5;
	ev2 *= 0.5;

	return std::min(ev1, ev2);
}